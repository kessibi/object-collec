FROM postgres:alpine

ENV POSTGRES_DB collec

ENV POSTGRES_PASSWORD postgres

ADD models/CREATE_DB.sql /docker-entrypoint-initdb.d/s1.sql
ADD models/CREATE_PROCEDURES.sql /docker-entrypoint-initdb.d/s2.sql
ADD models/CREATE_TRIGGERS.sql /docker-entrypoint-initdb.d/s3.sql


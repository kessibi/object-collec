-- 1: Users who have created a list for each type of objects
-- .. books, music, movies
SELECT u.first, u.last, u.username FROM Users u
INNER JOIN list a
ON a.user_id = u.id
INNER JOIN list b
ON a.user_id = b.user_id
INNER JOIN list c
ON b.user_id = c.user_id
WHERE a.typ = 1 AND b.typ = 2 AND c.typ = 3;

-- 2: Objects having more than 20 comments and their average rating being above 14
SELECT a.name FROM Objects a
INNER JOIN Comments b
ON a.id = b.o_id
INNER JOIN obj_rat c
ON a.id = c.o_id
GROUP BY a.id
HAVING COUNT(b.id) > 20 AND AVG(c.rating) > 14;

-- 3: Users who never gave a rating below 8
SELECT distinct a.id, a.first, a.last, a.username FROM Users a
INNER JOIN obj_rat b
ON a.id = b.u_id
WHERE b.rating > 8;

-- 4: Most commented object from last week
SELECT * FROM Objects 
WHERE id = (SELECT o_id 
    FROM Comments c 
    GROUP BY o_id 
    ORDER BY count(o_id) DESC 
    FETCH first 1 row only);

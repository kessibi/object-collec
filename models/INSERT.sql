-- Categories
INSERT INTO Genre VALUES (1, 1, 'RAP', 'De la musique pas très incroyable');
INSERT INTO Genre VALUES (2, 1, 'Disco', 'De la musique pas très incroyable');
INSERT INTO Genre VALUES (3, 1, 'Soul', 'De la musique pas très incroyable');
INSERT INTO Genre VALUES (4, 1, 'Electro', 'De la musique pas très incroyable');
INSERT INTO Genre VALUES (5, 1, 'House', 'De la musique pas très incroyable');
INSERT INTO Genre VALUES (6, 1, 'Hip Hop', 'De la musique pas très incroyable');
INSERT INTO Genre VALUES (7, 2, 'Action/Aventure', 'Du film pas très incroyable');
INSERT INTO Genre VALUES (8, 3, 'Gotic', 'Du livre pas très incroyable');
INSERT INTO Genre VALUES (9, 3, 'SF', 'Du livre pas très incroyable');
INSERT INTO Genre VALUES (10, 3, 'SF', 'Du livre pas très incroyable');
INSERT INTO Genre VALUES (11, 3, 'Crime', 'Du livre pas très incroyable');


-- Musics
SELECT add_music('DoggyStyle', 1, '1993', '{{Snoop, Dogg}, {Dr. Dre, /}, {Daz, Dillinger}}');
SELECT add_music('The Chronic', 1, '1993', '{{Dr. Dre, /}, {Suge, Knight}}');
SELECT add_music('Get In Where You Fit In', 1, '1993', '{{Too $hort, /}, {Ant, Banks}}');
SELECT add_music('Risque', 2, '1979', '{{Bernard, Edwards}, {Nile, Rodgers}}');
SELECT add_music('Diamond Life', 3, '1983', '{{Sade, /}}');
SELECT add_music('T69 Collapse', 4, '2018', '{{Aphex Twin, /}}');
SELECT add_music('Lovelee Dae', 5, '1997', '{{Blaze, /}}');
SELECT add_music('Homework', 5, '1997', '{{Daft Punk, /}}');
SELECT add_music('The Whistle Song', 5, '1991', '{{Frankie, Knuckles}}');
SELECT add_music('The Money Store', 6, '2012', '{{Death Grips, /}}');


-- Books
SELECT add_book('The Fellowship of the Ring', 'Fantasy', 7,'1954',
  'The Lord of the Rings', '{{J. R. R., Tolkien}}');

SELECT add_book('The Two Towers', 'Fantasy', 7, '1954',
  'The Lord of the Rings', '{{J. R. R., Tolkien}}');

SELECT add_book('The Return of the King', 'Fantasy', 8, '1955',
  'The Lord of the Rings', '{{J. R. R., Tolkien}}');

SELECT add_book('The Hobbit', 'Fantasy', 7, '1937',
  'The Lord of the Rings', '{{J. R. R., Tolkien}}');

SELECT add_book('To Kill a Mocking Bird', 'Roman', 8, '1960',
  '', '{{Harper, Lee}}');

SELECT add_book('Pride and Prejudice', 'Roman', 8, '1812',
  '', '{{Jane, Austen}}');

SELECT add_book('Le Rouge et le Noir', 'Novel', 9, '1830',
  '', '{{Stendhal, /}}');

SELECT add_book('Le blanc, le noir et le truand', 'Novel', 9, '2015',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter I', 'Roman', 9, '1930',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter II', 'Roman', 9, '1929',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter III', 'Roman', 9, '1931',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter IV', 'Roman', 9, '2018',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter V', 'Roman', 9, '1915',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter VI', 'Roman', 9, '1967',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter VII', 'Roman', 9, '1976',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter VIII', 'Roman', 9, '1833',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter IX', 'Roman', 9, '1836',
  '', '{{Stendhal, /}}');

SELECT add_book('Harry Potter X', 'Roman', 9, '1833',
  '', '{{Stendhal, /}}');

SELECT add_book('Monsieur le délégué', 'Novel', 8, '1830',
  '', '{{Stendhal, /}}');

SELECT add_book('Chocogabiche et les mormonts', 'Novel', 8, '1332',
  '', '{{Stendhal, /}}');

SELECT add_book('Le pirate', 'Novel', 9, '1239',
  '', '{{Stendhal, /}}');

SELECT add_book('Lecrivain qui ne sait pas lire', 'Novel', 9, '1232',
  '', '{{Stendhal, /}}');

SELECT add_book('Le bon, la bête et le fiché S', 'Novel', 8, '1720',
  '', '{{Stendhal, /}}');

SELECT add_book('Alice au pays du Wonderland', 'Novel', 9, '1940',
  '', '{{Stendhal, /}}');

SELECT add_book('Comment planter un bananier en Alsace', 'Novel', 8, '1430',
  '', '{{Stendhal, /}}');

SELECT add_book('Ma vie de pirate', 'Novel', 8, '2007',
  '', '{{Stendhal, /}}');

SELECT add_book('Comment utiliser une carte bancaire', 'Novel', 9, '2017',
  '', '{{Stendhal, /}}');

SELECT add_book('Le guide du fetard', 'Novel', 9, '0',
  '', '{{Stendhal, /}}');

SELECT add_book('Comment contruire une barque', 'Novel', 8, '1',
  '', '{{Stendhal, /}}');

SELECT add_book('La base des données', 'Novel', 9, '1131',
  '', '{{Stendhal, /}}');

SELECT add_book('Faire un site de professionel avec wordpress', 'Novel', 9, '1200',
  '', '{{Stendhal, /}}');

SELECT add_book('Apprendre le PHP en 30 secondes', 'Novel', 8, '1823',
  '', '{{Stendhal, /}}');

SELECT add_book('Moi, Michel et mon chien', 'Novel', 9, '1820',
  '', '{{Stendhal, /}}');


-- Movies
SELECT add_movie(
  'Blade Runner',
  10,
  '1982',
  '{{Ridley, Scott}, {Hampton, Fancher}, {David, Webb Peoples}}',
  '{{Harrison, Ford}, {Rutger, Hauer}, {Sean, Young}, {Daryl, Hannah}}'
);

SELECT add_movie(
  'The Matrix',
  10,
  '1999',
  '{{Lana, Wachowski}, {Larry, Wachoswki}}',
  '{{Keanu, Reeves}, {Laurence, Fishburne}, {Carrie-Anne, Moss}}'
);

SELECT add_movie(
  'Pulp Fiction',
  11,
  '1994',
  '{{Quentin, Tarantino}}',
  '{{John, Travolta}, {Samuel, L. Jackson}, {Uma, Thurman}, {Bruce, Willis}}'
);

SELECT add_movie(
  'Monsieur le délégué',
  11,
  '1974',
  '{{ G, Gabin }}',
  '{{ G, Gabin}, { G, Nicolas}, {Uma, Tata}, {Bruce, Jaques}}'
);

SELECT add_movie(
  'Le bon, la bête et la secrétaire',
  10,
  '2017',
  '{{ Saint, berttrand }}',
  '{{ L, Joey }, { Michel, Jackson }}'
);


SELECT add_movie(
  'Les tuches 3',
  11,
  '2018',
  '{{ Tuche, Yves }}',
  '{{ P, Joey }, { ZOO, Jeannine }}'
);

-- Objects List
SELECT create_list(2, 2, 'Good Rap Music', 'An effective description');
SELECT create_list(2, 1, 'the good books', 'The true good movies');
SELECT create_list(2, 3, 'the good others', 'The true good movies');


-- Add Objects to lists
SELECT add_elts_list(2, 2, '(2, Nice album overall)');
SELECT add_elts_list(2, 2, '(1, Gotta love snoop doggy dogg)');
SELECT add_elts_list(2, 2, '(3,)');
SELECT add_elts_list(2, 2, '(5, If you wantsend me a description)');
SELECT add_elts_list(2, 2, '(8, The good item)');
SELECT add_elts_list(2, 2, '(10,)');

-- Comments
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (2, 1, 'This album is just incredible, Snoop Dogg really touched my heart.', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (4, 2, 'I prefer watch TPMP ...', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (10, 3, 'Not incredible', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (11, 1, 'This was a good time', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (16, 7, 'Waste of money, better to buy an Iphone', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (15, 1, 'The story is not complete', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (3, 3, 'I was happy when i bought this product, now i dont', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 1, 'Better to write yourself', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (9, 4, 'Null', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (6, 4, 'The website work perfectly', current_date);

INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 4, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 3, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 2, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 5, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 8, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 9, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 10, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 11, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 12, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 13, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 14, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 15, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 40, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 42, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 43, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 44, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 45, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 46, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 23, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 33, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 31, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 22, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 27, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (5, 49, 'The website work perfectly', current_date);

INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 4, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 3, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 2, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 5, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 8, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 9, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 10, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 11, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 12, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 13, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 14, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 15, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 40, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 42, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 43, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 44, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 45, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 46, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 23, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 33, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 31, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 22, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 27, 'The website work perfectly', current_date);
INSERT INTO Comments (o_id, u_id, description, iat) VALUES (7, 49, 'The website work perfectly', current_date);

-- Rating
SELECT rate_object(1, 2, 18);
SELECT rate_object(3, 2, 7);
SELECT rate_object(2, 2, 11);
SELECT rate_object(2, 4, 1);
SELECT rate_object(4, 9, 19);
SELECT rate_object(4, 5, 14);
SELECT rate_object(3, 3, 7);
SELECT rate_object(5, 2, 3);
SELECT rate_object(7, 8, 16);
SELECT rate_object(8, 7, 18);
SELECT rate_object(8, 5, 20);
SELECT rate_object(9, 4, 15);
SELECT rate_object(10, 4, 17);
SELECT rate_object(11, 5, 16);
SELECT rate_object(12, 7, 18);
SELECT rate_object(13, 7, 17);
SELECT rate_object(14, 5, 19);


SELECT rate_object(15, 1, 19);
SELECT rate_object(15, 2, 19);
SELECT rate_object(15, 3, 19);
SELECT rate_object(15, 4, 19);
SELECT rate_object(15, 5, 19);
SELECT rate_object(15, 6, 19);
SELECT rate_object(15, 7, 19);
SELECT rate_object(15, 8, 19);
SELECT rate_object(15, 9, 19);
SELECT rate_object(15, 10, 19);
SELECT rate_object(15, 11, 19);
SELECT rate_object(15, 12, 19);
SELECT rate_object(15, 13, 19);
SELECT rate_object(15, 14, 19);
SELECT rate_object(15, 15, 19);
SELECT rate_object(15, 16, 19);
SELECT rate_object(15, 17, 19);
SELECT rate_object(15, 18, 19);
SELECT rate_object(15, 19, 19);
SELECT rate_object(15, 20, 19);
SELECT rate_object(15, 21, 19);
SELECT rate_object(15, 22, 19);
SELECT rate_object(15, 23, 19);
SELECT rate_object(15, 24, 19);
SELECT rate_object(15, 25, 19);
SELECT rate_object(15, 26, 19);
SELECT rate_object(15, 27, 19);
SELECT rate_object(15, 28, 19);
SELECT rate_object(15, 29, 19);
SELECT rate_object(15, 30, 19);
SELECT rate_object(15, 31, 19);
SELECT rate_object(15, 32, 19);
SELECT rate_object(15, 33, 19);
SELECT rate_object(15, 34, 19);
SELECT rate_object(15, 35, 19);
SELECT rate_object(15, 36, 19);
SELECT rate_object(15, 37, 19);
SELECT rate_object(15, 38, 19);
SELECT rate_object(15, 39, 19);
SELECT rate_object(15, 40, 19);
SELECT rate_object(15, 41, 19);
SELECT rate_object(15, 42, 19);
SELECT rate_object(15, 43, 19);

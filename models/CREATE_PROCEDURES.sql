CREATE OR REPLACE FUNCTION register(fir VARCHAR(64), las VARCHAR(64),
    addr VARCHAR(256), bir date, pass VARCHAR(256))

  RETURNS Integer AS $$
DECLARE
  a TEXT := LEFT(fir, 1);
  b TEXT := LEFT(las, 7);
  u_id integer;
BEGIN
  a := lower(a) || lower(b);
  IF(pass !~* '^[1-9a-z_]+$') THEN
    RAISE EXCEPTION 'Invalid password';
  END IF;
  b := crypt(pass, gen_salt('bf', 10));
  FOR i IN 0..9 LOOP
    FOR j IN 0..9 LOOP
      IF NOT EXISTS(SELECT * FROM Users WHERE username = (a || i || j)) THEN
        INSERT INTO Users(username, password, first, last, address, birth, signup)
        VALUES(a || i || j, b, initcap(lower(fir)), upper(las), addr, bir, current_date) RETURNING id INTO u_id;
        RETURN u_id;
      END IF;
    END LOOP;
  END LOOP;
  RAISE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION login(usernam VARCHAR(10), passwd VARCHAR(256))
  RETURNS integer AS $$
BEGIN
  IF EXISTS(SELECT * FROM Users WHERE username = usernam AND
      password = crypt(passwd, password)) THEN
    
    RETURN id; 
  END IF;
  
  RETURN -1;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION rate_object(uid INTEGER, obj INTEGER, rat INTEGER)
  RETURNS BOOLEAN AS $$
BEGIN
  IF EXISTS(SELECT * FROM obj_rat WHERE o_id = obj AND u_id = uid) THEN
    UPDATE obj_rat SET rating = rat WHERE o_id = obj AND u_id = uid;
  
  ELSIF EXISTS(SELECT * FROM Objects where id = obj)
    AND EXISTS(SELECT * FROM Users where id = uid) THEN
    INSERT INTO obj_rat VALUES(obj, uid, rat);
  
  ELSE RETURN FALSE;
  END IF;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_actors(list VARCHAR(64)[])
  RETURNS BOOLEAN AS $$
BEGIN
  FOR i IN 1..array_length(list, 1) LOOP
    IF list[i][2] = '/' THEN
      IF NOT EXISTS(SELECT * FROM People where lower(first) like lower(list[i][1])) THEN
        INSERT INTO People(id, first) VALUES(nextval('people_seq'), list[i][1]);
        INSERT INTO movie_actors VALUES(currval('people_seq'), currval('obj_seq'));
      END IF;
    ELSE
      IF NOT EXISTS(SELECT * FROM People where lower(first) like lower(list[i][1])
          AND lower(last) like lower(list[i][2])) THEN
        INSERT INTO People VALUES(nextval('people_seq'), list[i][1], list[i][2]);
        INSERT INTO movie_actors VALUES(currval('people_seq'), currval('obj_seq'));
      END IF;
    END IF;
  END LOOP;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_authors(list VARCHAR(64)[])
  RETURNS BOOLEAN AS $$
BEGIN
  FOR i IN 1..array_length(list, 1) LOOP
    IF list[i][2] = '/' THEN
      IF NOT EXISTS(SELECT * FROM People where lower(first) like lower(list[i][1])) THEN
        INSERT INTO People(id, first) VALUES(nextval('people_seq'), list[i][1]);
        INSERT INTO obj_authors VALUES(currval('people_seq'), currval('obj_seq'));
      END IF;
    ELSE
      IF NOT EXISTS(SELECT * FROM People where lower(first) like lower(list[i][1])
          AND lower(last) like lower(list[i][2])) THEN
        INSERT INTO People VALUES(nextval('people_seq'), list[i][1], list[i][2]);
        INSERT INTO obj_authors VALUES(currval('people_seq'), currval('obj_seq'));
      END IF;
    END IF;
  END LOOP;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_music(nam VARCHAR(64), genr INTEGER, year VARCHAR(4), auth VARCHAR(64)[])
  RETURNS BOOLEAN AS $$
BEGIN
  IF EXISTS(SELECT * FROM Objects where lower(name) like lower(nam) and typ = 2) THEN
    RETURN FALSE;
  END IF;
  INSERT INTO Objects VALUES(nextval('obj_seq'), 2, nam, year);
  INSERT INTO Music VALUES(currval('obj_seq'), genr);
  PERFORM add_authors(auth);
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_book(nam VARCHAR(64), styl VARCHAR(64), genr INTEGER,
year VARCHAR(4), collection VARCHAR(64), auth VARCHAR(64)[])
  RETURNS BOOLEAN AS $$
BEGIN
  IF EXISTS(SELECT * FROM Objects where lower(name) like lower(nam) and typ = 1) THEN
    RETURN FALSE;
  END IF;
  INSERT INTO Objects VALUES(nextval('obj_seq'), 1, nam, year);
  INSERT INTO Book VALUES(currval('obj_seq'), styl, genr, collection);
  PERFORM add_authors(auth);
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_movie(nam VARCHAR(64), genr INTEGER,
  year VARCHAR(4), auth VARCHAR(64)[], actors VARCHAR(64)[])
  
  RETURNS BOOLEAN AS $$
BEGIN
  IF EXISTS(SELECT * FROM Objects where lower(name) like lower(nam) and typ = 3) THEN
    RETURN FALSE;
  END IF;
  INSERT INTO Objects VALUES(nextval('obj_seq'), 3, nam, year);
  INSERT INTO Movie VALUES(currval('obj_seq'), genr);
  PERFORM add_authors(auth);
  PERFORM add_actors(actors);
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_elts_list(l_id INTEGER, u_id INTEGER, objs obj_list_t)
  RETURNS BOOLEAN AS $$
BEGIN

  IF NOT EXISTS(
    SELECT a.* FROM Users a
    INNER JOIN List b ON b.user_id = a.id
    WHERE a.id = u_id AND b.id = l_id) THEN
    
    RETURN FALSE;

  ELSIF NOT EXISTS(
    SELECT a.* from Objects a
    INNER JOIN List b ON b.id = l_id
    WHERE a.id = objs.o_id AND b.typ = a.typ
  ) THEN
    RAISE NOTICE 'The object does not exist'; 
    RETURN FALSE;

  ELSIF EXISTS(SELECT * from obj_list where list_id = l_id AND obj_id = objs.o_id) THEN
    RETURN FALSE;
  END IF;
  INSERT INTO obj_list VALUES(l_id, objs.o_id, objs.descr);
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION create_list(u_id INTEGER, typ INTEGER,
  listnam VARCHAR(128), description VARCHAR(1024))
  
  RETURNS INTEGER AS $$
DECLARE
 l_id INTEGER;
BEGIN
  IF NOT EXISTS(SELECT * FROM Users where id = u_id) THEN
    RETURN EXCEPTION 'User does not exist';
  ELSIF EXISTS(SELECT * FROM List where name = listnam AND user_id = u_id) THEN
    RETURN -1;
  ELSIF char_length(listnam) < 2  THEN
    RETURN -1;
  ELSIF typ < 1 OR typ > 3 THEN
    RETURN -1;
  END IF;
  INSERT INTO List (typ, name, description, user_id)
    VALUES(typ, listnam, description, u_id) RETURNING id INTO l_id;
  RETURN l_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION avg_rating(obj INTEGER)
  RETURNS INTEGER AS $$
DECLARE
  c integer := 0;
BEGIN
  SELECT COUNT(rating) INTO c FROM obj_rat WHERE o_id = obj;

  IF c < 20 THEN
    RETURN 0;
  ELSE
    SELECT AVG(rating) INTO c FROM obj_rat WHERE o_id = obj;
    return c;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION favorites(user_id INTEGER) RETURNS INTEGER AS
$$
DECLARE
  rec RECORD;
  res INTEGER;
BEGIN
  -- already tests for user
  res := create_list(user_id, 2, 'Favorite Music', 'Your top 10 Music');
  
  IF res = -1 then
    RAISE EXCEPTION 'Could not create list';
  END IF; 
 
  FOR rec IN
    SELECT r.* from obj_rat r INNER JOIN Objects o 
    ON o.id = r.o_id where r.u_id = user_id AND o.typ = 2
    ORDER BY r.rating DESC FETCH FIRST 10 ROW ONLY
  LOOP
    INSERT INTO obj_list VALUES(res, rec.o_id, '');
  END LOOP;
  
  res := create_list(user_id, 1, 'Favorite Books', 'Your top 10 Books');
  
  IF res = -1 then
    RAISE EXCEPTION 'Could not create list';
  END IF; 
 
  FOR rec IN
    SELECT r.* from obj_rat r INNER JOIN Objects o 
    ON o.id = r.o_id where r.u_id = user_id AND o.typ = 1
    ORDER BY r.rating DESC FETCH FIRST 10 ROW ONLY
  LOOP
    INSERT INTO obj_list VALUES(res, rec.o_id, '');
  END LOOP;
  
  res := create_list(user_id, 3, 'Favorite Movies', 'Your top 10 Movies');
  
  IF res = -1 then
    RAISE EXCEPTION 'Could not create list';
  END IF; 
 
  FOR rec IN
    SELECT r.* from obj_rat r INNER JOIN Objects o 
    ON o.id = r.o_id where r.u_id = user_id AND o.typ = 3
    ORDER BY r.rating DESC FETCH FIRST 10 ROW ONLY
  LOOP
    INSERT INTO obj_list VALUES(res, rec.o_id, '');
  END LOOP;

  RETURN 1;
END;
$$ LANGUAGE plpgsql;

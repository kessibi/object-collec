CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE Users(
  id SERIAL PRIMARY KEY,
  username VARCHAR(10) UNIQUE,
  password VARCHAR(256),
  first VARCHAR(64), -- first name
  last VARCHAR(64), -- last name
  address VARCHAR(256),
  birth DATE,
  signup DATE
);

CREATE INDEX user_id ON Users(id);

CREATE TABLE Objects(
  id SERIAL PRIMARY KEY UNIQUE,
  typ INTEGER CHECK(typ IN (1, 2, 3)),
  name VARCHAR(64) NOT NULL,
  year VARCHAR(4) NOT NULL
  -- 1: book, 2: music:, 3: movie
);

CREATE INDEX obj_id ON Objects(id, typ);

-- We consider that one person can post multiple comments under
-- a single object
CREATE TABLE Comments(
  id SERIAL PRIMARY KEY,
  o_id INTEGER references Objects(id),
  u_id SERIAL references Users(id),
  description TEXT,
  iat date
);

CREATE TABLE Genre(
  id SERIAL PRIMARY KEY,
  type INTEGER CHECK(type IN (1,2,3)),
  name VARCHAR(64),
  description VARCHAR(64)
);

CREATE TABLE Music(
  id INTEGER PRIMARY KEY REFERENCES Objects(id),
  genre INTEGER REFERENCES Genre(id)
);

CREATE TABLE Book(
  id INTEGER PRIMARY KEY REFERENCES Objects(id),
  style VARCHAR(32) NOT NULL,
  genre INTEGER REFERENCES Genre(id),
  collection VARCHAR(64)
);
-- for movies, directors will be saved as authors
CREATE TABLE Movie(
  id INTEGER PRIMARY KEY REFERENCES Objects(id),
  genre INTEGER REFERENCES Genre(id)
);

CREATE TABLE People(
  id SERIAL PRIMARY KEY UNIQUE,
  first VARCHAR(64) NOT NULL,
  last VARCHAR(64)
);

CREATE INDEX people_id ON People(id);

CREATE TABLE obj_authors(
  auth_id INTEGER REFERENCES People(id),
  obj_id INTEGER REFERENCES Objects(id),
  PRIMARY KEY(auth_id, obj_id)
);

CREATE TABLE movie_actors(
  act_id INTEGER REFERENCES People(id),
  film_id INTEGER REFERENCES Movie(id),
  PRIMARY KEY(act_id, film_id)
);

CREATE TABLE List(
  id SERIAL PRIMARY KEY UNIQUE,
  typ INTEGER CHECK(typ IN (1, 2, 3)),
  name VARCHAR(128) NOT NULL,
  description VARCHAR(1024),
  user_id SERIAL REFERENCES Users(id)
);

CREATE INDEX list_id ON List(id, typ);

CREATE TYPE obj_list_t AS (
  o_id INTEGER,
  descr VARCHAR(256)
);

CREATE TABLE obj_list(
  list_id INTEGER REFERENCES List(id),
  obj_id INTEGER REFERENCES Objects(id),
  description VARCHAR(256),
  PRIMARY KEY (list_id, obj_id)
);

CREATE TABLE obj_rat(
  o_id INTEGER REFERENCES Objects(id),
  u_id SERIAL REFERENCES Users(id),
  rating INTEGER CHECK(rating > 0 AND rating <= 20),
  PRIMARY KEY (o_id, u_id)
);

CREATE SEQUENCE obj_seq START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE people_seq START WITH 1 INCREMENT BY 1;

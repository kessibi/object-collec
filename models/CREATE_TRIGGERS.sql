CREATE OR REPLACE FUNCTION comments_check() RETURNS TRIGGER AS
$$
DECLARE
  a Comments%ROWTYPE;
BEGIN
  SELECT * from Comments INTO a where o_id = NEW.o_id order by id desc limit 1;
  if a.u_id = NEW.u_id then
    UPDATE Comments SET description = a.description || NEW.description where id = a.id;
    RETURN NULL; 
  else
    RETURN NEW;
  end if;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER T_COMMENTS
  BEFORE INSERT
  ON Comments FOR EACH ROW
  EXECUTE FUNCTION comments_check();

CREATE OR REPLACE FUNCTION news() RETURNS TRIGGER AS
$$
DECLARE
  l_id text := date_part('month', now()) || '_' || date_part('year', now());
  u_id integer;
  news_id integer;
BEGIN
  -- nnews00: The news account with playlists
  select id into news_id from users where username = 'nnews00';
  
  if not exists(select id from list where name = l_id and user_id = news_id) then
    INSERT INTO List (typ, name, description, user_id)
    VALUES (NEW.typ, l_id, 'Playlist du mois ' || l_id, 1) returning id into u_id;
  end if;
  select id into u_id from list where name = l_id and user_id = news_id;
  raise notice '% %', l_id, u_id;
  INSERT INTO obj_list (list_id, obj_id) VALUES (u_id, NEW.id);
  
  RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER T_NEWS
  AFTER INSERT
  ON Objects FOR EACH ROW
  EXECUTE FUNCTION news();
